import { Routes } from '@angular/router';
import { InicioComponent } from '../inicio/inicio.component';
import { SalaComponent } from '../sala/sala.component';
import { UsuarioComponent } from '../usuario/usuario.component';
import { PuertaComponent } from '../puerta/puerta.component';

export const appRoutes: Routes = [
    { path: 'inicio', component: InicioComponent },
    { path: 'sala', component: SalaComponent },
    { path: 'usuario', component: UsuarioComponent },
    { path: 'puerta', component: PuertaComponent},
    //Redireccionan al inicio
    {
        path: '',
        redirectTo: '/inicio',
        pathMatch: 'full'
    },
    //rederiza a la pg 404 cuando no hay un componente solicitado
    //{ path: '**', component: PageNotFoundComponent }
];