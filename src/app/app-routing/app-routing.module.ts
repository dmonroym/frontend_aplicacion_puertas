import { CommonModule } from "@angular/common";
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { appRoutes } from './routes';



@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]

})

export class AppRoutingModule { }