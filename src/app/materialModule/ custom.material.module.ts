import {
  MatButtonModule, MatCheckboxModule,
  MatToolbarModule, MatCardModule, MatDividerModule,
  MatSelectModule, MatFormFieldModule,
  MatInputModule
} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';

import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule
  ],
  exports: [
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule
  ],
})
export class CustomMaterialModule { }