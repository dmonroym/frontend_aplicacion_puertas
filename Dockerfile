# Usar  imagen de nodejs
FROM node
# Crear carpeta de trabajo
RUN mkdir -p /usr/src/app
# seleccionar carpeta de trabajo
WORKDIR /usr/src/app
# copiar archivos del cliente
COPY . .
# instalar blibliotecas
RUN npm install -g @angular/cli
#RUN npm install -i -f 
RUN npm install
# abrir puerto
EXPOSE 4200
# ejecutar comando para iniciar la API
CMD ng serve --host 0.0.0.0 --disable-host-check
